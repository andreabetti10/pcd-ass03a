package pcd.ass03a.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import pcd.ass03a.view.StartView;

public class StartListener implements ActionListener {
	
	private final StartView startPanel = new StartView(this);

	@Override
	public void actionPerformed(ActionEvent e) {
		new Thread(() -> {
			final int playersNum = startPanel.getPlayersNum();
			final int digitsNum = startPanel.getDigitsNum();
			final boolean userPlayer = startPanel.getUserPlays();
			new Controller(playersNum, digitsNum, userPlayer);
		}).start();

	}

}
