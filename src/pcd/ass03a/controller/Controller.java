package pcd.ass03a.controller;

import akka.actor.ActorSystem;
import akka.actor.Props;
import pcd.ass03a.controller.actors.RefereeActor;

public class Controller {
	
	private final ActorSystem system;
	
	public Controller(int numPlayers, int numDigits, boolean userPlayer) {
		system = ActorSystem.create("CollectiveMastermind");
		system.actorOf(Props.create(RefereeActor.class, numPlayers, numDigits, userPlayer), "referee");
	}
	

}
