package pcd.ass03a.controller.actors;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import pcd.ass03a.controller.messages.StartMsg;
import pcd.ass03a.controller.messages.TurnMsg;
import pcd.ass03a.controller.messages.VictoryMsg;
import pcd.ass03a.controller.messages.WrongSolutionMsg;
import pcd.ass03a.view.GameView;
import pcd.ass03a.controller.messages.CorrectSolutionMsg;
import pcd.ass03a.controller.messages.DefeatMsg;
import pcd.ass03a.controller.messages.ForwardGuessMsg;
import pcd.ass03a.controller.messages.ForwardSolutionMsg;
import pcd.ass03a.controller.messages.GuessMsg;
import pcd.ass03a.controller.messages.NewGameMsg;
import pcd.ass03a.controller.messages.NextTurnMsg;
import pcd.ass03a.controller.messages.ReadyMsg;
import pcd.ass03a.controller.messages.ResetMsg;
import pcd.ass03a.controller.messages.SolutionsMsg;

public class RefereeActor extends AbstractActor implements ActionListener {

	private static final long GUESS_TIMEOUT = TimeUnit.SECONDS.toMillis(5);

	private final GameView gameView;

	private final List<ActorRef> players = new ArrayList<>();
	private Stack<Integer> turnOrder;
	private Map<Integer, ActorRef> activePlayers = new HashMap<>();
	private Timer timer;
	private TimerTask guessTimeoutTask;
	private int nextPlayerId;
	private Optional<Integer> playerToWait = Optional.empty();
	private boolean isRight = false;
	private int turn = 0;
	private int receivedSolutionFeedbacks = 0;
	private int receivedReadyAcks = 0;
	private boolean activeGame = false;
	private Map<Integer, String> playerNames = new HashMap<>();

	public RefereeActor(int numPlayers, int numDigits, boolean userPlayer) {
		gameView = new GameView(userPlayer, numPlayers, numDigits);
		gameView.addNewActionListener(this);
		gameView.addStopActionListener(this);

		if (userPlayer) {
			playerNames.put(numPlayers, "User");
		}
		for (int index = 0; index < numPlayers; index++) {
			playerNames.put(index, "Player " + index);
		}

		for (int index = 0; index < numPlayers; index++) {
			// Keeps references to player actors, each player takes the referee ref in the
			// constructor
			final ActorRef player = getContext().actorOf(Props.create(PlayerActor.class, index,
					userPlayer ? numPlayers + 1 : numPlayers, numDigits, getSelf(), playerNames, gameView),
					"player" + index);
			players.add(player);
			activePlayers.put(index, player);
		}
		if (userPlayer) {
			final ActorRef user = getContext().actorOf(
					Props.create(UserActor.class, numPlayers, numDigits, getSelf(), playerNames, gameView),
					"player" + numPlayers);
			players.add(user);
			activePlayers.put(numPlayers, user);
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(StartMsg.class, msg -> {

			activePlayers = new HashMap<>();
			for (int index = 0; index < players.size(); index++) {
				activePlayers.put(index, players.get(index));
			}
			turnOrder = new Stack<>();
			timer = new Timer();
			isRight = true;
			turn = 0;
			receivedSolutionFeedbacks = 0;
			receivedReadyAcks = 0;
			activeGame = true;
			getSelf().tell(new NextTurnMsg(), getSelf());
			gameView.setStopEnabled(true);

		}).match(NextTurnMsg.class, msg -> {

			if (activeGame) {
				turn++;
				gameView.printToLog("Turn " + turn);
				nextPlayerId = popNextPlayer(turnOrder, activePlayers);
				gameView.printToLog("Next player is " + playerNames.get(nextPlayerId));
				playerToWait = Optional.of(nextPlayerId);
				guessTimeoutTask = new TimerTask() {
					@Override
					public void run() {
						if (playerToWait.isPresent()) {
							gameView.printToLog("Guess Timeout for " + playerNames.get(playerToWait.get())
									+ ", guesses from him at wrong turn will be ignored");
							getSelf().tell(new NextTurnMsg(), getSelf());
						}
					}
				};
				final long timeOut = nextPlayerId == (players.size() - 1) ? GUESS_TIMEOUT * 6 : GUESS_TIMEOUT;
				timer.schedule(guessTimeoutTask, timeOut);
				activePlayers.get(nextPlayerId).tell(new TurnMsg(nextPlayerId, turn, System.currentTimeMillis()),
						getSelf());
			} else {
				getSelf().tell(new ResetMsg(), getSelf());
			}

		}).match(GuessMsg.class, msg -> {

			if (activeGame) {
				final long timeElapsed = System.currentTimeMillis() - msg.getGuessStartTime();
				gameView.printToLog(playerNames.get(msg.getGuesserPlayerId()) + " has sent the guess " + msg.getGuess()
						+ " to the referee for " + msg.getGuessedPlayerId());
				if (msg.getGuesserPlayerId() != nextPlayerId || msg.getTurnNum() < turn) {
					gameView.printToLog(timeElapsed + " Late guess from " + playerNames.get(msg.getGuesserPlayerId())
							+ ", ignored (for guess timeout)");
				} else {
					if (playerToWait.isPresent() && playerToWait.get().equals(msg.getGuesserPlayerId())) {
						playerToWait = Optional.empty();
						stopTimer();
					}
					gameView.printToLog("Time elapsed: " + timeElapsed);
					gameView.printToLog(playerNames.get(msg.getGuesserPlayerId()) + " can guess number of "
							+ msg.getGuessedPlayerId());
					// The referee will forward the guess to the specified
					// player with a list with
					// actor references to every other player, to send them the
					// guess result
					players.get(msg.getGuessedPlayerId()).tell(new ForwardGuessMsg(msg.getGuess(),
							msg.getGuessedPlayerId(), msg.getGuesserPlayerId(), players), getSelf());
				}
			}

		}).match(SolutionsMsg.class, msg -> {

			if (activeGame) {
				final long timeElapsed = System.currentTimeMillis() - msg.getGuessStartTime();
				if (msg.getPlayerId() != nextPlayerId || msg.getTurnNum() < turn) {
					gameView.printToLog(timeElapsed + " Late solution from " + playerNames.get(msg.getPlayerId())
							+ ", ignored (for guess timeout)");
				} else {
					gameView.printToLog(playerNames.get(msg.getPlayerId()) + " is attempting a solution");
					for (int i = 0; i < players.size(); i++) {
						if (i != msg.getPlayerId()) {
							players.get(i).tell(new ForwardSolutionMsg(msg.getPlayerId(), msg.getSolutionFor(i)),
									getSelf());
						}
					}
				}
			}

		}).match(CorrectSolutionMsg.class, msg -> {

			if (activeGame) {
				receivedSolutionFeedbacks++;
				if (receivedSolutionFeedbacks == players.size() - 1) {
					receivedSolutionFeedbacks = 0;
					if (isRight) {
						gameView.printToLog(playerNames.get(msg.getPlayerId()) + "'s solution is correct");
						players.get(msg.getPlayerId()).tell(new VictoryMsg(), getSelf());
					} else {
						if (!eliminatePlayer(msg.getPlayerId())) {
							isRight = true;
							players.get(msg.getPlayerId()).tell(new DefeatMsg(), getSelf());
						}
					}
				}
			}

		}).match(WrongSolutionMsg.class, msg -> {

			if (activeGame) {
				receivedSolutionFeedbacks++;
				if (receivedSolutionFeedbacks == players.size() - 1) {
					receivedSolutionFeedbacks = 0;
					if (!eliminatePlayer(msg.getPlayerId())) {
						if (playerToWait.equals(Optional.of(msg.getPlayerId()))) {
							playerToWait = Optional.empty();
							stopTimer();
						}
						players.get(msg.getPlayerId()).tell(new DefeatMsg(), getSelf());
					}
				} else {
					isRight = false;
				}
			}

		}).match(ReadyMsg.class, msg -> {

			if (activeGame) {
				receivedReadyAcks++;
				gameView.printToLog(playerNames.get(msg.getPlayer()) + " is ready for turn " + (turn + 1) + " ("
						+ receivedReadyAcks + "/" + activePlayers.size() + ")");
				if (receivedReadyAcks == activePlayers.size()) {
					receivedReadyAcks = 0;
					getSelf().tell(new NextTurnMsg(), getSelf());
				}
			}

		}).match(ResetMsg.class, msg -> {

			activeGame = false;
			gameView.printToLog("Resetting...");
			playerToWait = Optional.empty();
			stopTimer();
			players.forEach(e -> e.tell(new ResetMsg(), getSelf()));

		}).match(NewGameMsg.class, msg -> {

			receivedReadyAcks++;
			if (receivedReadyAcks == players.size()) {
				receivedReadyAcks = 0;
				gameView.setNewEnabled(true);
				gameView.setStopEnabled(false);
			}

		}).build();
	}

	/*
	 * returns true if game is over
	 */
	private boolean eliminatePlayer(int playerId) {
		gameView.printToLog(playerNames.get(playerId) + " has been eliminated");
		activePlayers.remove(playerId);
		turnOrder.remove((Integer) playerId);
		if (activePlayers.size() == 1) {
			final Entry<Integer, ActorRef> winner = activePlayers.entrySet().iterator().next();
			gameView.printToLog(playerNames.get(winner.getKey()) + " wins at the table!");
			winner.getValue().tell(new VictoryMsg(), getSelf());
			playerToWait = Optional.empty();
			stopTimer();
			return true;
		}
		return false;
	}

	private int popNextPlayer(Stack<Integer> turnOrder, Map<Integer, ActorRef> activePlayers) {
		if (turnOrder.isEmpty()) {
			final List<Integer> ids = activePlayers.keySet().stream().collect(Collectors.toList());
			Collections.shuffle(ids);
			turnOrder.addAll(ids);
		}
		return turnOrder.pop();
	}

	private void stopTimer() {
		guessTimeoutTask.cancel();
		timer.purge();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("New Game")) {
			getSelf().tell(new StartMsg(), getSelf());
		} else if (e.getActionCommand().equals("Stop")) {
			getSelf().tell(new ResetMsg(), getSelf());
		}
	}

}