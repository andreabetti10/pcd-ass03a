package pcd.ass03a.controller.actors;

import static pcd.ass03a.controller.CombinationsUtils.checkGuess;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.japi.Pair;
import pcd.ass03a.controller.messages.CorrectSolutionMsg;
import pcd.ass03a.controller.messages.DefeatMsg;
import pcd.ass03a.controller.messages.ForwardGuessMsg;
import pcd.ass03a.controller.messages.ForwardSolutionMsg;
import pcd.ass03a.controller.messages.GuessMsg;
import pcd.ass03a.controller.messages.GuessResultMsg;
import pcd.ass03a.controller.messages.NewGameMsg;
import pcd.ass03a.controller.messages.NextTurnMsg;
import pcd.ass03a.controller.messages.ReadyMsg;
import pcd.ass03a.controller.messages.ResetMsg;
import pcd.ass03a.controller.messages.SolutionsMsg;
import pcd.ass03a.controller.messages.TurnMsg;
import pcd.ass03a.controller.messages.VictoryMsg;
import pcd.ass03a.controller.messages.WrongSolutionMsg;
import pcd.ass03a.view.GameView;

public class UserActor extends AbstractActor implements ActionListener {

	private static final int DIGITS_UPPERBOUND = 10;
	private final Random rand = new Random();

	private final GameView gameView;

	private final int id;
	private final int numDigits;
	private final ActorRef referee;
	private boolean alive;
	private Optional<Long> currentGuessStartTime = Optional.empty();
	private Optional<Integer> currentTurnNum = Optional.empty();
	
	private final Map<Integer, String> playerNames;

	private List<Integer> secretNumber;

	public UserActor(int id, int numDigits, ActorRef referee, Map<Integer, String> playerNames, GameView gameView) {
		this.id = id;
		this.numDigits = numDigits;
		this.referee = referee;
		this.playerNames = playerNames;
		this.gameView = gameView;
		this.gameView.addGuessActionListener(this);
		this.gameView.addSolutionActionListener(this);
		getSelf().tell(new ResetMsg(), getSelf());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(TurnMsg.class, msg -> {

			currentTurnNum = Optional.of(msg.getTurnNum());
			currentGuessStartTime = Optional.of(msg.getGuessStartTime());
			if (alive) {
				gameView.setGuessEnabled(true);
				gameView.setSolEnabled(true);
			}
			
		}).match(ForwardGuessMsg.class, msg -> {

			final Pair<Integer, Integer> result = checkGuess(msg.getGuess(), secretNumber);
			gameView.printToLog("User received the guess: Right place " + result.first() + ", wrong place "
					+ result.second() + ", Guess: " + msg.getGuess() + " Secret number: " + secretNumber);

			// The guessed player sends the result to other players
			for (int i = 0; i < msg.getPlayers().size(); i++) {
				if (i != id) {
					msg.getPlayers().get(i).tell(new GuessResultMsg(msg.getGuess(), msg.getGuessedPlayerId(),
							msg.getGuesserPlayerId(), result.first(), result.second()), getSelf());
				}
			}

			if (alive) {
				referee.tell(new ReadyMsg(id), getSelf());
			}

		}).match(GuessResultMsg.class, msg -> {

			if (alive) {
				gameView.printToLog("User has received the result of " + playerNames.get(msg.getGuessedPlayerId()) + " guessed by "
						+ playerNames.get(msg.getGuesserPlayerId()) + " (guess: " + msg.getGuess() + ", right place: "
						+ msg.getRightPlace() + ", wrong place: " + msg.getWrongPlace() + ")");
			}
			
			referee.tell(new ReadyMsg(id), getSelf());

		}).match(ForwardSolutionMsg.class, msg -> {

			if (secretNumber.equals(msg.getSolution())) {
				gameView.printToLog("User knows that Player " + msg.getPlayerId() + " is right!" + " Answer: "
						+ msg.getSolution() + " Solution: " + secretNumber);
				referee.tell(new CorrectSolutionMsg(msg.getPlayerId()), getSelf());
			} else {
				gameView.printToLog("User knows that Player " + msg.getPlayerId() + " is wrong!" + " Answer: "
						+ msg.getSolution() + " Solution: " + secretNumber);
				referee.tell(new WrongSolutionMsg(msg.getPlayerId()), getSelf());
			}

		}).match(DefeatMsg.class, msg -> {

			gameView.printToLog("User is out!");
			alive = false;
			referee.tell(new NextTurnMsg(), getSelf());

		}).match(VictoryMsg.class, msg -> {

			gameView.printToLog("You win!");
			referee.tell(new ResetMsg(), getSelf());

		}).match(ResetMsg.class, msg -> {

			alive = true;
			secretNumber = new ArrayList<>();
			for (int i = 0; i < numDigits; i++) {
				secretNumber.add(rand.nextInt(DIGITS_UPPERBOUND));
			}
			gameView.printToLog("User is ready for the next game");
			referee.tell(new NewGameMsg(id), getSelf());

		}).build();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (alive && currentTurnNum.isPresent() && currentGuessStartTime.isPresent()) {
			if (e.getActionCommand().equals("Guess")) {
				// Player sends guess to referee, the referee will check if there is guess timeout
				referee.tell(new GuessMsg(gameView.getGuess(), gameView.getGuessedPlayerId(), id, currentTurnNum.get(),
						currentGuessStartTime.get()), getSelf());
			} else if (e.getActionCommand().equals("Solve")) {
				// If the player thinks that he has won at the beginning of its turn, it sends his solution
				referee.tell(new SolutionsMsg(gameView.getSolutions(), id, currentTurnNum.get(), currentGuessStartTime.get()),
						getSelf());
			}
		}		
	}

}
