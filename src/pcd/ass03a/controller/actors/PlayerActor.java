package pcd.ass03a.controller.actors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.japi.Pair;
import pcd.ass03a.controller.messages.GuessMsg;
import pcd.ass03a.controller.messages.GuessResultMsg;
import pcd.ass03a.controller.messages.NewGameMsg;
import pcd.ass03a.controller.messages.NextTurnMsg;
import pcd.ass03a.controller.messages.ReadyMsg;
import pcd.ass03a.controller.messages.ResetMsg;
import pcd.ass03a.controller.messages.SolutionsMsg;
import pcd.ass03a.controller.messages.TurnMsg;
import pcd.ass03a.controller.messages.VictoryMsg;
import pcd.ass03a.controller.messages.WrongSolutionMsg;
import pcd.ass03a.view.GameView;
import pcd.ass03a.controller.messages.CorrectSolutionMsg;
import pcd.ass03a.controller.messages.DefeatMsg;
import pcd.ass03a.controller.messages.ForwardGuessMsg;
import pcd.ass03a.controller.messages.ForwardSolutionMsg;

import static pcd.ass03a.controller.CombinationsUtils.*;

public class PlayerActor extends AbstractActor {

	private static final int TIMEOUT_CHANCE = 10; // chance 1 over... to take too much time to guess a combination
	private static final int ERROR_CHANCE = 2; // chance 1 over... to give an incorrect solution

	private static final int DIGITS_UPPERBOUND = 10;
	private final Random rand = new Random();

	private final GameView gameView;

	private final int id;
	private final int numDigits;
	private final int numPlayers;
	private final ActorRef referee;
	private boolean alive;
	
	private final Map<Integer, String> playerNames;

	private List<Integer> secretNumber;

	// Stores unused combinations for each opponent - they will decrease at each
	// guess
	private Map<Integer, List<List<Integer>>> opponentsCombinations;
	// Stores candidate solutions for each opponent
	private Map<Integer, List<List<Integer>>> candidateSolutions;
	// Stores current guess for each opponent
	private Map<Integer, List<Integer>> currentGuesses;

	public PlayerActor(int id, int numPlayers, int numDigits, ActorRef referee, Map<Integer, String> playerNames, GameView gameView) {
		this.id = id;
		this.numPlayers = numPlayers;
		this.numDigits = numDigits;
		this.referee = referee;
		this.playerNames = playerNames;
		this.gameView = gameView;
		getSelf().tell(new ResetMsg(), getSelf());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(TurnMsg.class, msg -> {

			if (alive) {

				// Chance it could timeout
				if (new Random().nextInt(TIMEOUT_CHANCE) == 0) {
					gameView.printToLog("Player " + id + " feels sleepy");
					Thread.sleep(TimeUnit.SECONDS.toMillis(6));
				}

				// If the player thinks that he has won at the beginning of its turn, it sends
				// his solution
				if (candidateSolutions.entrySet().stream().filter(e -> e.getValue().size() <= 1)
						.count() == candidateSolutions.entrySet().size()) {

					final Map<Integer, List<Integer>> solutions = new HashMap<>();
					currentGuesses.forEach((k, v) -> solutions.put(k, v));

					// Chance it could go wrong
					if (new Random().nextInt(ERROR_CHANCE) == 0) {
						gameView.printToLog("Player " + id + " feels distracted");
						final int index = IntStream.range(0, solutions.size()).filter(e -> e != id).findAny()
								.getAsInt();
						solutions.get(index).clear();
						for (int i = 0; i < numDigits; i++) {
							solutions.get(index).add(rand.nextInt(DIGITS_UPPERBOUND));
						}
					}

					referee.tell(new SolutionsMsg(solutions, id, msg.getTurnNum(), msg.getGuessStartTime()), getSelf());

				} else {

					// Take entry from the ones with less candidate solutions
					candidateSolutions.entrySet().stream().filter(e -> e.getValue().size() > 1)
							.sorted((l1, l2) -> l1.getValue().size() - l2.getValue().size()).findFirst()
							.ifPresent(entry -> {
								final int guessedPlayerId = entry.getKey();
								gameView.printToLog("Player " + id + " guesses " + currentGuesses.get(guessedPlayerId)
										+ " for " + playerNames.get(guessedPlayerId));

								// Player sends guess to referee, the referee will check if there is guess
								// timeout
								referee.tell(new GuessMsg(currentGuesses.get(guessedPlayerId), guessedPlayerId, id,
										msg.getTurnNum(), msg.getGuessStartTime()), getSelf());
							});

				}
			}

		}).match(ForwardGuessMsg.class, msg -> {

			final Pair<Integer, Integer> result = checkGuess(msg.getGuess(), secretNumber);
			gameView.printToLog(
					"Player " + id + " has received the guess: Right place " + result.first() + ", wrong place "
							+ result.second() + ", Guess: " + msg.getGuess() + " Secret number: " + secretNumber);

			// The guessed player sends the result to other players
			for (int i = 0; i < msg.getPlayers().size(); i++) {
				if (i != id) {
					msg.getPlayers().get(i).tell(new GuessResultMsg(msg.getGuess(), msg.getGuessedPlayerId(),
							msg.getGuesserPlayerId(), result.first(), result.second()), getSelf());
				}
			}

			if (alive) {
				referee.tell(new ReadyMsg(id), getSelf());
			}

		}).match(GuessResultMsg.class, msg -> {

			if (alive) {

				// The used guess is removed from used combinations for the guessed player

				final Pair<Integer, Integer> guessResult = checkGuess(secretNumber, msg.getGuess());
				if (guessResult.first().equals(numDigits) && guessResult.second().equals(0)) {
					candidateSolutions.get(msg.getGuessedPlayerId()).removeIf(e -> !e.equals(msg.getGuess()));
				} else {
					gameView.printToLog("Player " + id + " has received the result of " + playerNames.get(msg.getGuessedPlayerId())
							+ " guessed by " + playerNames.get(msg.getGuesserPlayerId()) + " (guess: " + msg.getGuess()
							+ ", right place: " + msg.getRightPlace() + ", wrong place: " + msg.getWrongPlace() + ")");

					if (candidateSolutions.entrySet().stream().filter(e -> e.getValue().size() <= 1)
							.count() < candidateSolutions.size()) {

						// Removing any code that would not give the same response if it were the code
						if (candidateSolutions.get(msg.getGuessedPlayerId()).size() > 1) {
							candidateSolutions.get(msg.getGuessedPlayerId()).removeIf(e -> {
								final Pair<Integer, Integer> currentResult = checkGuess(msg.getGuess(), e);
								return !(currentResult.first().equals(msg.getRightPlace())
										&& currentResult.second().equals(msg.getWrongPlace()));
							});
						}

						final Map<Pair<Integer, Integer>, Integer> scoreCount = new HashMap<>();
						final Map<List<Integer>, Integer> score = new HashMap<>();

						// Computing scores for combinations
						for (final List<Integer> combination : opponentsCombinations.get(msg.getGuessedPlayerId())) {
							for (final List<Integer> solution : candidateSolutions.get(msg.getGuessedPlayerId())) {
								final Pair<Integer, Integer> pegScore = checkGuess(combination, solution);
								if (scoreCount.containsKey(pegScore)) {
									scoreCount.put(pegScore, scoreCount.get(pegScore) + 1);
								} else {
									scoreCount.put(pegScore, 1);
								}
							}
							final int max = scoreCount.isEmpty() ? 0 : Collections.max(scoreCount.values());
							score.put(combination, max);
							scoreCount.clear();
						}

						final int min = score.isEmpty() ? Integer.MAX_VALUE : Collections.min(score.values());

						final Map<Integer, List<List<Integer>>> nextGuesses = new HashMap<>();
						for (int i = 0; i < opponentsCombinations.size() + 1; i++) {
							if (i != id) {
								nextGuesses.put(i, new ArrayList<>());
							}
						}

						nextGuesses.get(msg.getGuessedPlayerId()).clear();
						score.forEach((k, v) -> {
							if (v.equals(min)) {
								nextGuesses.get(msg.getGuessedPlayerId()).add(k);
							}
						});

						// Setting the current guesses
						final Optional<List<Integer>> g1 = nextGuesses.get(msg.getGuessedPlayerId()).stream()
								.filter(g -> candidateSolutions.get(msg.getGuessedPlayerId()).contains(g)).findFirst();
						if (!g1.isPresent()) {
							currentGuesses.put(msg.getGuessedPlayerId(),
									nextGuesses.get(msg.getGuessedPlayerId()).stream().filter(
											g -> opponentsCombinations.get(msg.getGuessedPlayerId()).contains(g))
											.findFirst().orElse(new ArrayList<>()));
						} else {
							currentGuesses.put(msg.getGuessedPlayerId(), g1.get());
						}
					}
				}

				// The player says that he's ready after having
				// calculated its candidate solutions
				referee.tell(new ReadyMsg(id), getSelf());

			} else {
				gameView.printToLog(
						"Player " + id + " received the result of a guess but is not interested since is out");
			}

		}).match(ForwardSolutionMsg.class, msg -> {

			if (secretNumber.equals(msg.getSolution())) {
				gameView.printToLog("Player " + id + " knows that " + playerNames.get(msg.getPlayerId()) + " is right!"
						+ " Answer: " + msg.getSolution() + " Solution: " + secretNumber);
				referee.tell(new CorrectSolutionMsg(msg.getPlayerId()), getSelf());
			} else {
				gameView.printToLog("Player " + id + " knows that " + playerNames.get(msg.getPlayerId()) + " is wrong!"
						+ " Answer: " + msg.getSolution() + " Solution: " + secretNumber);
				referee.tell(new WrongSolutionMsg(msg.getPlayerId()), getSelf());
			}

		}).match(DefeatMsg.class, msg -> {

			gameView.printToLog("Player " + id + " knows that he's out!");
			alive = false;
			referee.tell(new NextTurnMsg(), getSelf());

		}).match(VictoryMsg.class, msg -> {

			gameView.printToLog("Player " + id + " knows that he has won!");
			referee.tell(new ResetMsg(), getSelf());

		}).match(ResetMsg.class, msg -> {

			alive = true;
			opponentsCombinations = new HashMap<>();
			candidateSolutions = new HashMap<>();
			currentGuesses = new HashMap<>();
			secretNumber = new ArrayList<>();
			for (int i = 0; i < numDigits; i++) {
				secretNumber.add(rand.nextInt(DIGITS_UPPERBOUND));
			}
			for (int i = 0; i < numPlayers; i++) {
				if (i != id) {
					opponentsCombinations.put(i, getAllCombinations(numDigits, DIGITS_UPPERBOUND));
					candidateSolutions.put(i, getAllCombinations(numDigits, DIGITS_UPPERBOUND));
					currentGuesses.put(i, getInitialGuess(numDigits, DIGITS_UPPERBOUND));
				}
			}

			gameView.printToLog("Player " + id + " is ready for the next game");
			referee.tell(new NewGameMsg(id), getSelf());

		}).build();
	}

}