package pcd.ass03a.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import akka.japi.Pair;

public class CombinationsUtils {
	
	/*
	 * Get initial guess, random number
	 */
	public static List<Integer> getInitialGuess(int numDigits, int digitUpperbound) {
		final List<Integer> guess = new ArrayList<>();
		final Random rand = new Random();
		for (int i = 0; i < numDigits; i++) {
			guess.add(rand.nextInt(digitUpperbound));
		}
		return guess;
	}

	public static Pair<Integer, Integer> checkGuess(List<Integer> secretNumber, List<Integer> secretNumber2) {
		int rightPlace = 0;
		int wrongPlace = 0;
		if (secretNumber.size() == secretNumber2.size()) {
			final int numDigits = secretNumber.size();
			final List<Boolean> used = new ArrayList<>(Collections.nCopies(numDigits, false));
			// Right place
			for (int i = 0; i < numDigits; ++i) {
				if (secretNumber.get(i) == secretNumber2.get(i)) {
					rightPlace++;
					used.set(i, true);
				}
			}
			// Wrong place
			for (int i = 0; i < numDigits; ++i) { // guess slots
				for (int j = 0; j < numDigits; ++j) { // answer slots
					// If this slot in answer has not been previouly used,
					if (!used.get(j) && secretNumber.get(i) == secretNumber2.get(j)) {
						wrongPlace++;
						used.set(i, true);
						// Now that this guess slot has been "bound" to
						// an answer slot, don't try to match it to
						// another answer slot
						break;
					}
				}
			}
		}
		return new Pair<>(rightPlace, wrongPlace);
	}
	
	/*
	 * Return every possible combination
	 */
	public static List<List<Integer>> getAllCombinations(int numDigits, int digitUpperbound) {
		final List<Integer> current = new ArrayList<>(Collections.nCopies(numDigits, 0));
		final List<Integer> elements = IntStream.range(0, digitUpperbound)
				.boxed().collect(Collectors.toList());
		
		final List<List<Integer>> combinations = new ArrayList<>();
		combinationRecursive(combinations, numDigits, 0, current, elements);
		return combinations;
	}
	
	private static void combinationRecursive(List<List<Integer>> combinations, int numDigits, int position, List<Integer> current, List<Integer> elements) {
		if (position >= numDigits) {
			combinations.add(current);
			return;
		}
		
		for (int i = 0; i < elements.size(); ++i) {
			final List<Integer> currentClone = current.stream().collect(Collectors.toList());
			currentClone.set(position, elements.get(i));
	        combinationRecursive(combinations, numDigits, position + 1, currentClone, elements);
	    }
	}

}
