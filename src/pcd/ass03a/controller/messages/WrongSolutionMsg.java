package pcd.ass03a.controller.messages;

public class WrongSolutionMsg {

	private final int playerId;

	public WrongSolutionMsg(int playerId) {
		this.playerId = playerId;
	}

	public int getPlayerId() {
		return playerId;
	}

}
