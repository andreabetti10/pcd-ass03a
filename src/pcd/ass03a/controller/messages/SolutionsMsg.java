package pcd.ass03a.controller.messages;

import java.util.List;
import java.util.Map;

public class SolutionsMsg {
	
	private final Map<Integer, List<Integer>> solutions;
	private final int playerId;
	private final int turnNum;
	private final long guessStartTime;
	
	public SolutionsMsg(Map<Integer, List<Integer>> solutions, int playerId, int turnNum, long guessStartTime) {
		this.solutions = solutions;
		this.playerId = playerId;
		this.turnNum = turnNum;
		this.guessStartTime = guessStartTime;
	}

	public List<Integer> getSolutionFor(int player) {
		return solutions.get(player);
	}
	
	public int getPlayerId() {
		return playerId;
	}
	
	public int getTurnNum() {
		return turnNum;
	}
	
	public long getGuessStartTime() {
		return guessStartTime;
	}
	
}
