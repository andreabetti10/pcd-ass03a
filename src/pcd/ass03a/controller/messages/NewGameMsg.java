package pcd.ass03a.controller.messages;

public class NewGameMsg {
	
private final int player;
	
	public NewGameMsg(int player) {
		this.player = player;
	}

	public int getPlayer() {
		return player;
	}

}
