package pcd.ass03a.controller.messages;

import java.util.List;

import akka.actor.ActorRef;

public class ForwardGuessMsg {

	private final List<Integer> guess;
	private final int guessedPlayerId;
	private final int guesserPlayerId;
	private final List<ActorRef> players;

	public ForwardGuessMsg(List<Integer> guess, int guessedPlayerId,
			int guesserPlayerId, List<ActorRef> players) {
		this.guess = guess;
		this.guessedPlayerId = guessedPlayerId;
		this.guesserPlayerId = guesserPlayerId;
		this.players = players;
	}

	public List<Integer> getGuess() {
		return guess;
	}

	public int getGuessedPlayerId() {
		return guessedPlayerId;
	}

	public int getGuesserPlayerId() {
		return guesserPlayerId;
	}

	public List<ActorRef> getPlayers() {
		return players;
	}

}
