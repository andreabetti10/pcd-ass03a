package pcd.ass03a.controller.messages;

public class ReadyMsg {
	
	private final int player;
	
	public ReadyMsg(int player) {
		this.player = player;
	}

	public int getPlayer() {
		return player;
	}

}
