package pcd.ass03a.controller.messages;

import java.util.List;

public class GuessResultMsg {

	private final List<Integer> guess;
	private final int guessedPlayerId;
	private final int guesserPlayerId;
	private final int rightPlace;
	private final int wrongPlace;

	public GuessResultMsg(List<Integer> guess, int guessedPlayerId,
			int guesserPlayerId, int rightPlace, int wrongPlace) {
		this.guess = guess;
		this.guessedPlayerId = guessedPlayerId;
		this.guesserPlayerId = guesserPlayerId;
		this.rightPlace = rightPlace;
		this.wrongPlace = wrongPlace;
	}

	public List<Integer> getGuess() {
		return guess;
	}

	public int getGuessedPlayerId() {
		return guessedPlayerId;
	}

	public int getGuesserPlayerId() {
		return guesserPlayerId;
	}

	public int getRightPlace() {
		return rightPlace;
	}

	public int getWrongPlace() {
		return wrongPlace;
	}

}
