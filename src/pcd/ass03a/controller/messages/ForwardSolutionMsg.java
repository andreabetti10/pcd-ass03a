package pcd.ass03a.controller.messages;

import java.util.List;

public class ForwardSolutionMsg {
	
	private final int playerId;
	private final List<Integer> solution;
	
	public ForwardSolutionMsg(int playerId, List<Integer> solution) {
		this.playerId = playerId;
		this.solution = solution;
	}
	
	public int getPlayerId() {
		return playerId;
	}
	
	public List<Integer> getSolution() {
		return solution;
	}

}
