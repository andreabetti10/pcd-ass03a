package pcd.ass03a.controller.messages;

import java.util.List;

public class GuessMsg {

	private final List<Integer> guess;
	private final int guessedPlayerId;
	private final int guesserPlayerId;
	private final int turnNum;
	private final long guessStartTime;

	public GuessMsg(List<Integer> guess, int guessedPlayerId,
			int guesserPlayerId, int turnNum, long guessStartTime) {
		this.guess = guess;
		this.guessedPlayerId = guessedPlayerId;
		this.guesserPlayerId = guesserPlayerId;
		this.turnNum = turnNum;
		this.guessStartTime = guessStartTime;
	}

	public List<Integer> getGuess() {
		return guess;
	}

	public int getGuessedPlayerId() {
		return guessedPlayerId;
	}
	
	public int getGuesserPlayerId() {
		return guesserPlayerId;
	}
	
	public int getTurnNum() {
		return turnNum;
	}

	public long getGuessStartTime() {
		return guessStartTime;
	}

}
