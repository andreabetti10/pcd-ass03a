package pcd.ass03a.controller.messages;

public class TurnMsg {

	private final int playerId;
	private final int turnNum;
	private final long guessStartTime;

	public TurnMsg(int playerId, int turnNum, long guessStartTime) {
		this.playerId = playerId;
		this.turnNum = turnNum;
		this.guessStartTime = guessStartTime;
	}

	public int getPlayerId() {
		return playerId;
	}
	
	public int getTurnNum() {
		return turnNum;
	}

	public long getGuessStartTime() {
		return guessStartTime;
	}

}
