package pcd.ass03a.controller.messages;

public class CorrectSolutionMsg {
	
	private final int playerId;

	public CorrectSolutionMsg(int playerId) {
		this.playerId = playerId;
	}

	public int getPlayerId() {
		return playerId;
	}

}
