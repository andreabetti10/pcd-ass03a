package pcd.ass03a.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class StartView extends JFrame implements ActionListener {

	private static final long serialVersionUID = -538738764475236905L;
	
	private final JLabel cpuLabel = new JLabel("CPUs: ");
	private final JSpinner cpuSelector = new JSpinner(new SpinnerNumberModel(3, 2, null, 1));
	private final JLabel digitLabel = new JLabel("Digits: ");
	private final JSpinner digitSelector = new JSpinner(new SpinnerNumberModel(3, 1, null, 1));
	private final JCheckBox userPlayer = new JCheckBox("I want to play");
	private final JButton startButton = new JButton("Start");

	public StartView(ActionListener actionListener) {
		setTitle("Mastermind");
		setSize(256, 96);
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
			@Override
			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		final JPanel p = new JPanel();
		p.add(cpuLabel);
		p.add(cpuSelector);
		p.add(digitLabel);
		p.add(digitSelector);
		p.add(userPlayer);
		p.add(startButton);
		getContentPane().add(p);
		setVisible(true);
		startButton.addActionListener(this);
		startButton.addActionListener(actionListener);
	}

	public int getPlayersNum() {
		return (Integer) cpuSelector.getValue();
	}

	public int getDigitsNum() {
		return (Integer) digitSelector.getValue();
	}

	public boolean getUserPlays() {
		return userPlayer.isSelected();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		startButton.setEnabled(false);
		this.setVisible(false);
	}

}
