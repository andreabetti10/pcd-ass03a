package pcd.ass03a.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;

public class GameView extends JFrame implements ActionListener {

	private static final long serialVersionUID = 7190021672610390859L;

	private final JTextArea eventLog = new JTextArea();
	private final JScrollPane sp = new JScrollPane(eventLog);
	private final JButton stopButton = new JButton("Stop");
	private final JButton newButton = new JButton("New Game");
	private final JLabel guessLabel = new JLabel("Guess: ");
	private final JTextField guess = new JTextField();
	private final JLabel playerLabel = new JLabel("Player: ");
	private final JComboBox<String> player = new JComboBox<>();
	private final JButton guessButton = new JButton("Guess");
	private final List<JTextField> solutions = new ArrayList<>();
	private final JButton solButton = new JButton("Solve");
	
	private final int digits;

	public GameView(boolean userPlayer, int CPUs, int digits) {
		this.digits = digits;
		setTitle("Mastermind");
		setSize(1024, 384);
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}

			@Override
			public void windowClosed(WindowEvent ev) {
				System.exit(-1);
			}
		});
		final JPanel p = new JPanel();
		sp.setPreferredSize(new Dimension(992, 256));
		newButton.addActionListener(e -> newButton.setEnabled(false));
		stopButton.addActionListener(e -> {
			stopButton.setEnabled(false);
			guessButton.setEnabled(false);
			solButton.setEnabled(false);
		});
		guessButton.addActionListener(e -> {
			guessButton.setEnabled(false);
			solButton.setEnabled(false);
		});
		solButton.addActionListener(e -> {
			guessButton.setEnabled(false);
			solButton.setEnabled(false);
		});
		newButton.setEnabled(false);
		stopButton.setEnabled(false);
		guessButton.setEnabled(false);
		solButton.setEnabled(false);
		p.add(sp);
		p.add(newButton);
		p.add(stopButton);
		if (userPlayer) {
			IntStream.range(0, CPUs).forEach(e -> player.addItem(Integer.toString(e)));
			PlainDocument doc = (PlainDocument) guess.getDocument();
			doc.setDocumentFilter(new MyIntFilter(digits));
			guess.setColumns(digits);
			p.add(guessLabel);
			p.add(guess);
			p.add(playerLabel);
			p.add(player);
			p.add(guessButton);
			for (int i = 0; i < CPUs; i++) {
				final JLabel solLabel = new JLabel("[" + i + "] solution: ");
				final JTextField sol = new JTextField();
				PlainDocument doc2 = (PlainDocument) sol.getDocument();
				doc2.setDocumentFilter(new MyIntFilter(digits));
				solutions.add(sol);
				sol.setColumns(digits);
				p.add(solLabel);
				p.add(sol);
			}
			p.add(solButton);
		}
		getContentPane().add(p);
		setVisible(true);
	}

	class MyIntFilter extends DocumentFilter {

		private int limit;

		public MyIntFilter(int limit) {
	        super();
	        this.limit = limit;
	    }

		@Override
		public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr)
				throws BadLocationException {

			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.insert(offset, string);

			if (sb.toString().length() > limit) {
				return;
			}

			if (test(sb.toString())) {
				super.insertString(fb, offset, string, attr);
			} else {
				printToLog("Only numeric characters allowed");
			}
		}

		private boolean test(String text) {
			if (text.trim().isEmpty()) {
				return true;
			}
			try {
				Integer.parseInt(text);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}

		@Override
		public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs)
				throws BadLocationException {

			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.replace(offset, offset + length, text);
			
			if (sb.toString().length() > limit) {
				return;
			}

			if (test(sb.toString())) {
				super.replace(fb, offset, length, text, attrs);
			} else {
				printToLog("Only numeric characters allowed");
			}

		}

		@Override
		public void remove(FilterBypass fb, int offset, int length) throws BadLocationException {
			Document doc = fb.getDocument();
			StringBuilder sb = new StringBuilder();
			sb.append(doc.getText(0, doc.getLength()));
			sb.delete(offset, offset + length);
			
			if (sb.toString().length() > limit) {
				return;
			}

			if (test(sb.toString())) {
				super.remove(fb, offset, length);
			} else {
				printToLog("Only numeric characters allowed");
			}

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		this.setVisible(false);
	}

	public void printToLog(String message) {
		SwingUtilities.invokeLater(() -> {
			eventLog.append(message + "\n");
			eventLog.setCaretPosition(eventLog.getDocument().getLength());
		});
	}

	public Map<Integer, List<Integer>> getSolutions() {
		final Map<Integer, List<Integer>> sols = new HashMap<>();
		solutions.forEach(e -> {
			final List<Integer> sol = new ArrayList<>();
			for (final Character c : e.getText().toCharArray()) {
				sol.add(Character.getNumericValue(c));
			}
			for (int i = guess.getText().length(); i < digits; i++) {
				sol.add(0);
			}
			sols.put(solutions.indexOf(e), sol);
		});
		return sols;
	}

	public List<Integer> getGuess() {
		final List<Integer> g = new ArrayList<>();
		for (final Character c : guess.getText().toCharArray()) {
			g.add(Character.getNumericValue(c));
		}
		for (int i = guess.getText().length(); i < digits; i++) {
			g.add(0);
		}
		return g;
	}

	public int getGuessedPlayerId() {
		return player.getSelectedIndex();
	}

	public void addNewActionListener(ActionListener listener) {
		newButton.addActionListener(listener);
	}

	public void addStopActionListener(ActionListener listener) {
		stopButton.addActionListener(listener);
	}

	public void addGuessActionListener(ActionListener listener) {
		guessButton.addActionListener(listener);
	}

	public void addSolutionActionListener(ActionListener listener) {
		solButton.addActionListener(listener);
	}

	public void setNewEnabled(boolean b) {
		newButton.setEnabled(b);
	}

	public void setStopEnabled(boolean b) {
		stopButton.setEnabled(b);
	}

	public void setGuessEnabled(boolean b) {
		guessButton.setEnabled(b);
	}

	public void setSolEnabled(boolean b) {
		solButton.setEnabled(b);
	}

}
